import errno
import socket
import time


class APP(object):
    def __init__(self, connection, client_address):
        self.connection = connection
        self.client_address = client_address
        self.imei = None
        self.heartbeat_timeout_seconds = 30
        self.start_bytes_list = ["78", "78"]
        self.end_bytes_list = ["0D", "0A"]

    def serve_client(self):
        response = ""
        start_time = time.time()
        while True:
            try:
                data = self.connection.recv(1)
                if data:
                    response += data
                    start_time = time.time()
                else:
                    if (time.time() - start_time) > 15:
                        print("No data: Timeout Occurred, exiting thread")
                        return None
                    else:
                        #  print("Device_{}: waiting for new data".format(self.imei))
                        time.sleep(1)

            except socket.error as e:
                err = e.args[0]
                #  print("err", e)
                if err == errno.EAGAIN or err == errno.EWOULDBLOCK:
                    if (time.time() - start_time) > self.heartbeat_timeout_seconds:
                        return None
                    else:
                        #  print("Device_{}: waiting for new data2".format(self.imei))
                        time.sleep(1)
            end_bytes_location = response.find("".join(self.end_bytes_list))
            #  print("end_bytes_location", end_bytes_location)
            if end_bytes_location > 0:
                start_bytes_location = response.find("".join(self.start_bytes_list))
                #  print("start_bytes_location", start_bytes_location)
                if start_bytes_location >= 0:
                    packet = response[start_bytes_location:end_bytes_location + len("".join(self.end_bytes_list)) + 1]
                    #  print("Got message: {}".format(packet))
                    if self.valid_crc(packet):
                        if packet[6:8] == "01":
                            #  login packet
                            self.send_login_response(packet)
                        elif packet[6:8] == "23" and self.imei is not None:
                            #  heartbeat packet
                            self.send_heartbeat_response(packet)
                        elif packet[6:8] == "22" and self.imei is not None:
                            #  gps packet
                            self.save_location_packet(packet)
                    response = response[end_bytes_location + len("".join(self.end_bytes_list)) + 1:]
                else:
                    return None

    def send_login_response(self, packet):
        self.imei = packet[8:24]
        print("\nReceived Login packet ( {} ) from Device : {}".format(packet, self.imei))
        response = packet[:8] + packet[-12:]
        self.connection.sendall(response)
        print("Sent Login Response: {} for Device: {}\n".format(response, self.imei))

    def send_heartbeat_response(self, packet):
        print("\nReceived heartbeat packet ( {} ) from Device : {}".format(packet, self.imei))
        response = packet[:8] + packet[-12:]
        self.connection.sendall(response)
        print("Sent HeartBeat Response: {} for Device: {}\n".format(response, self.imei))

    def save_location_packet(self, packet):
        print("\nReceived location packet ( {} ) from Device : {}".format(packet, self.imei))
        filename = "fileStorage/Device_{}.txt".format(self.imei)
        with open(filename, "a+") as fd:
            fd.write(packet+"\n")
        print("Saved location packet in file {}\n".format(filename))

    def valid_crc(self, packet):
        return True
