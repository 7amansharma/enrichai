import errno
import socket
import time

TERMINAL_ID_MAX_SIZE = 8
TYPE_ID_MAX_SIZE = 2
TIMEZONE_LANGUAGE_MAX_SIZE = 2
INFO_SERIAL_NUMBER_SIZE = 2
LOGIN_MSG_PACKET_LEN = 17
HEARTBEAT_MSG_PACKET_LEN = 11
LOCATION_MSG_PACKET_LEN = 38
TERMINAL_INFO_CONTENT_SIZE = 1
VOLTAGE_LEVEL_SIZE = 2
CRC_SIZE = 2


class Concox(object):
    def __init__(self, config):
        self.location_message_serial_number = 0
        self.heartbeat_message_serial_number = 0
        self.imei = config["device_imei_str"]
        self.type_id = config["device_type_id"]
        self.heartbeat_interval_seconds = config["heartbeat_interval_seconds"]
        self.location_update_interval = config["location_packet_update_interval"]
        self.sock = None
        self.timezone_offset = config["timezone_offset"]
        self.language = config["language"]
        self.terminal_id_bytes_list = self.str_to_bytes_list(self.imei, TERMINAL_ID_MAX_SIZE)
        self.type_id_bytes_list = self.str_to_bytes_list(self.type_id, TYPE_ID_MAX_SIZE)
        self.timezone_bytes_list = self.timezone_offset_to_timezone_bytes_list()
        self.login_message_serial_number = 0
        self.server_addr = (config["remote_server_host_name"], config["remote_server_port"])
        self.socket_timeout_when_no_data_from_server_secs = config["socket_timeout_when_no_data_from_server_secs"]
        self.server_connection_retry_time_secs = config["server_connection_retry_time_secs"]
        self.login_message_retry_time_secs = config["login_message_retry_time_secs"]
        self.sleep_time_interval_between_retries = config["sleep_time_interval_between_retries"]

    def get_crc_bytes_list(self, string):
        # crc algo
        return ["01", "01"]

    def timezone_offset_to_timezone_bytes_list(self):
        left_12_bits = 100 * abs(self.timezone_offset)
        left_12_bits = left_12_bits << 4
        bit3 = 0 if self.timezone_offset >= 0 else 1 << 3
        bit2 = 0
        bit1 = 0 if self.language == "english" else 1 << 1
        bit0 = 0
        hex_format = hex(left_12_bits | bit3 | bit2 | bit1 | bit0)
        hex_str = hex_format[2:]
        return self.str_to_bytes_list(hex_str, TIMEZONE_LANGUAGE_MAX_SIZE)

    def connect_to_server(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.sock.connect(self.server_addr)
            self.sock.setblocking(False)
        except Exception as e:
            print("Device_{} : failed to connect to the server: {}".format(self.imei, e))
            self.sock = None
            return False
        print("Device_{} : Connection to server success".format(self.imei))
        return True

    @staticmethod
    def str_to_bytes_list(string, max_size):
        hex_list = [string[i - 1:i + 1] for i in range(len(string) - 1, -1, -2)]
        #  print("hex_list", hex_list)
        if len(string) % 2 != 0:
            hex_list = hex_list[:-1]
            #  print("hex_list2", hex_list)
            hex_list.append("0" + string[0])
            #  print("hex_list3", hex_list)
        if len(hex_list) < max_size:
            for i in range(max_size - len(hex_list)):
                hex_list.append("00")
        hex_list.reverse()
        return hex_list

    def number_to_bytes_list(self, number, max_size):
        hex_str = hex(number)[2:]
        return self.str_to_bytes_list(hex_str, max_size)

    def get_login_packet(self):
        start_bits = ["78", "78"]
        packet_length = self.str_to_bytes_list(hex(LOGIN_MSG_PACKET_LEN)[2:], 1)
        protocol_number = ["01"]
        terminal_id = self.terminal_id_bytes_list
        type_id = self.type_id_bytes_list
        time_zone_language = self.timezone_bytes_list
        info_serial_number = self.number_to_bytes_list(self.login_message_serial_number, INFO_SERIAL_NUMBER_SIZE)
        crc = self.get_crc_bytes_list("".join(packet_length + protocol_number + terminal_id + type_id
                                              + time_zone_language + info_serial_number))
        stop_bits = ["0D", "0A"]
        return "".join(start_bits + packet_length + protocol_number + terminal_id + type_id + time_zone_language
                       + crc + stop_bits)

    def get_heartbeat_packet(self):
        start_bits = ["78", "78"]
        packet_length = self.str_to_bytes_list(hex(HEARTBEAT_MSG_PACKET_LEN)[2:], 1)
        protocol_number = ["23"]  # for heartbeat
        terminal_info_content = self.get_terminal_info_content_bytes_list()
        voltage_level = self.get_voltage_level_bytes_list()
        gsm_signal_strength = self.get_gsm_signal_strength_bytes_list()
        language = ["00", "02"]  # sample example
        info_serial_number = self.number_to_bytes_list(self.heartbeat_message_serial_number, INFO_SERIAL_NUMBER_SIZE)
        crc = self.get_crc_bytes_list("".join(packet_length + protocol_number + terminal_info_content + voltage_level
                                              + gsm_signal_strength + language + info_serial_number))
        stop_bits = ["0D", "0A"]
        return "".join(start_bits + packet_length + protocol_number + terminal_info_content + voltage_level
                       + gsm_signal_strength + language + crc + stop_bits)

    def get_location_packet(self):
        start_bits = ["78", "78"]
        packet_length = self.str_to_bytes_list(hex(LOCATION_MSG_PACKET_LEN)[2:], 1)
        protocol_number = ["22"]  # for location packet
        date_time = ["0F", "0C", "1D", "02", "33", "05"]
        gps_sat_quantity = ["C9"]
        lat = ["02", "7A", "C8", "18"]
        lon = ["0C", "46", "58", "60"]
        speed = ["00"]
        course = ["14", "00"]
        mcc = ["01", "CC"]
        mnc = ["00"]
        lac = ["28", "7D"]
        cell_id = ["00", "1F", "71"]
        acc = ["00"]
        data_upload_mode = ["00"]
        gps_real_time_upload_or_re_upload = ["01"]
        mileage = ["00", "00", "11", "22"]
        serial_number = self.number_to_bytes_list(self.location_message_serial_number, INFO_SERIAL_NUMBER_SIZE)
        crc = self.get_crc_bytes_list("")
        stop_bits = ["0D", "0A"]
        return "".join(start_bits + packet_length + protocol_number + date_time + gps_sat_quantity + lat + lon +
                       speed + course + mcc + mnc + lac + cell_id + acc + data_upload_mode
                       + gps_real_time_upload_or_re_upload + mileage + serial_number + crc + stop_bits)

    def send_login_message(self):
        if self.sock:
            login_msg = self.get_login_packet()
            try:
                self.sock.sendall(login_msg)
                print("\nSent Login Message ( {} ) for Device_{}".format(login_msg, self.imei))
            except Exception as e:
                print("\n****\nDevice_{}: Exception in sending login message : {}\n****\n".format(self.imei, e))
                return False
            self.login_message_serial_number += 1
            return True
        print("\n****\nDevice_{} : login msg not sent :connection is broken\n****\n".format(self.imei))
        return False

    def send_heartbeat_message(self):
        if self.sock:
            heartbeat_msg = self.get_heartbeat_packet()
            try:
                self.sock.sendall(heartbeat_msg)
                print("\nSent heartbeat Message ( {} ) for Device_{}".format(heartbeat_msg, self.imei))
            except Exception as e:
                print("\n****\nDevice_{}: Exception in sending heartbeat message : {}\n****\n".format(self.imei, e))
                return False
            self.heartbeat_message_serial_number += 1
            return True
        print("\n****\nDevice_{} : heartbeat not sent :connection is broken\n****\n".format(self.imei))
        return False

    def send_location_message(self):
        if self.sock:
            location_msg = self.get_location_packet()
            try:
                self.sock.sendall(location_msg)
                print("\nSent location Message ( {} ) for Device_{}".format(location_msg, self.imei))
            except Exception as e:
                print("\n****\nDevice_{}: Exception in sending location message : {}\n****\n".format(self.imei, e))
                return False
            self.heartbeat_message_serial_number += 1
            return True
        print("\n****\nDevice_{} : location msg not sent :connection is broken\n****\n".format(self.imei))
        return False

    def start_sending_location_data(self):
        ticks = 0
        last_location_sent_time = -1
        last_heartbeat_sent_time = -1
        while True:
            if ticks % self.heartbeat_interval_seconds == 0:
                self.sleep_time_interval_between_retries = 5
                heart_beat_success = False
                for sec in range(0, 15, self.sleep_time_interval_between_retries):  # wait for hearbeat for 5 min max
                    if self.send_heartbeat_message():
                        if not self.recv_heartbeat_response():
                            # heartbeat response failed
                            continue
                        else:
                            # heartbeat seems good, continue sending
                            last_heartbeat_sent_time = ticks
                            heart_beat_success = True
                            break
                    else:
                        # heartbeat sending failed
                        continue
                if not heart_beat_success:
                    # heartbeat sending failed
                    return
            if ticks % self.location_update_interval == 0:
                if not self.send_location_message():
                    # failed sending data
                    return
                else:
                    last_location_sent_time = ticks
            time.sleep(1)
            ticks += 1

    def recv_response(self, response_type, start_bytes_list, end_bytes_list, response_timeout_seconds=5):
        response = ""
        start_time = time.time()
        while True:
            try:
                data = self.sock.recv(1)
                if data:
                    response += data
                else:
                    if (time.time() - start_time) > self.socket_timeout_when_no_data_from_server_secs:
                        print("\n****\nDevice_{}: Timeout during receiving {} response\n****\n".format(self.imei,
                                                                                                       response_type))
                        return None
                    else:
                        #  print("Device_{}: waiting for response".format(self.imei))
                        time.sleep(1)
            except socket.error as e:
                err = e.args[0]
                #  print(err)
                if err == errno.EAGAIN or err == errno.EWOULDBLOCK:
                    if (time.time() - start_time) > self.socket_timeout_when_no_data_from_server_secs:
                        print("\n****\nDevice_{}: Timeout during receiving {} response\n****\n".format(self.imei,
                                                                                                       response_type))
                        return None
                    else:
                        #  print("Device_{}: waiting for response".format(self.imei))
                        time.sleep(1)
            end_bytes_location = response.find("".join(end_bytes_list))
            if end_bytes_location > 0:
                start_bytes_location = response.find("".join(start_bytes_list))
                if start_bytes_location >= 0:
                    return response[start_bytes_location:end_bytes_location + len("".join(end_bytes_list)) + 1]
                else:
                    return None

    def start_simulation(self):
        while True:  # reboot loop
            for sec in range(0, self.server_connection_retry_time_secs,
                             self.sleep_time_interval_between_retries):  # try connecting to server for 20 min max
                if not self.connect_to_server():
                    time.sleep(self.sleep_time_interval_between_retries)  # retry connecting after some time
                else:
                    # connection successful, exit retry loop
                    break
            if self.sock is not None:  # if connection object is not None
                for sec in range(0, self.login_message_retry_time_secs,
                                 self.sleep_time_interval_between_retries):  # try login for 20 min max
                    if not self.send_login_message():
                        time.sleep(self.sleep_time_interval_between_retries)
                    elif not self.recv_login_response():
                        continue
                    else:
                        # got login response
                        self.start_sending_location_data()
                        pass

            else:
                # reboot device as it was not able to connect to the sever in max time
                pass
            break
        # device starting and trying to establish connection

        pass

    def get_terminal_info_content_bytes_list(self):
        bit0 = 1 << 0  # Defense Activated
        bit1 = 1 << 1  # ACC high
        bit2 = 1 << 2  # Charge On
        bit3, bit4, bit5 = 0, 0, 0  # extended bits
        bit6 = 1 << 6  # GPS tracking is on
        bit7 = 0  # Oil and electricity connected
        result_byte = bit0 | bit1 | bit2 | bit3 | bit4 | bit5 | bit6 | bit7
        hex_str = hex(result_byte)[2:]
        return self.str_to_bytes_list(hex_str, TERMINAL_INFO_CONTENT_SIZE)

    def get_voltage_level_bytes_list(self):
        voltage_level = 4.15  # get voltage level
        hex_str = hex(int(voltage_level * 100))[2:]
        return self.str_to_bytes_list(hex_str, VOLTAGE_LEVEL_SIZE)

    @staticmethod
    def get_gsm_signal_strength_bytes_list():
        return ["03"]

    def recv_login_response(self):
        response_type = "login"
        response = self.recv_response(response_type=response_type, start_bytes_list=["78", "78"],
                                      end_bytes_list=["0D", "0A"])
        if response is not None:
            if self.crc_ok(response, response_type):
                print("Received Login Response ( {} ) from Device_{}\n".format(response, self.imei))
                return True
        else:
            print("\n****\nNo Login Response from server\n****\n")
            return False

    def recv_heartbeat_response(self):
        response_type = "heartbeat"
        response = self.recv_response(response_type=response_type, start_bytes_list=["78", "78"],
                                      end_bytes_list=["0D", "0A"])
        if response is not None:
            if self.crc_ok(response, response_type):
                print("Received HeartBeat Response ( {} ) from Device_{}\n".format(response, self.imei))
                return True
        else:
            print("\n****\nNo HeartBeat response from server\n****\n")
            return False

    def crc_ok(self, response, response_type):
        return True
