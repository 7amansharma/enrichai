import socket
import os
import threading
import time
import traceback
from classes.app import APP


class ConnectionHandler(threading.Thread):
    def __init__(self, thread_id, app):
        #  super(threading.Thread, self).__init__()
        threading.Thread.__init__(self)
        self.thread_id = thread_id
        self.app = app

    def run(self):
        try:
            self.app.serve_client()
        except Exception as e:
            print("e>>>>>>>>>>>>>>", e)


if __name__ == "__main__":
    while True:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_addr = ("localhost", 1234)
        sock.bind(server_addr)
        sock.listen(5)
        print("Server listening on {}:{}".format(server_addr[0], server_addr[1]))
        try:
            thread_id = 0
            threads = []
            while True:
                connection, client_address = sock.accept()
                print("\n\nReceived Connection from : {}\n\n".format(client_address))
                connection.setblocking(False)
                app = APP(connection, client_address)
                thread = ConnectionHandler(thread_id, app)
                thread_id += 1
                threads += [thread]
                thread.start()
        except Exception as e:
            print("Exception", e, ">>>", traceback.format_exc())
            print("Exit out of while loop")
            print("num threads alive ", len(list(threading.enumerate())))
        sock.close()
        break
        #  time.sleep(1)
