### deviceSimulator.py :
    This is the process responsible for simulating a Concox device.
    It contains the following configuration dict:
        config = {
            "device_imei_str": "123456789123456", ## this is the imei strings used in login packet  
            "device_type_id": "0001",  ## this is type id for the device used in login packet
            "heartbeat_interval_seconds": 5, ## interval between consecutive sucessful heartbeats
            "location_packet_update_interval": 5, ## interval between consecutive sucessful location packet transmission
            "timezone_offset": -5, ## It sets the timezone for device, example GMT-5hrs will be -5.
            "language":"english",  ## language of the device, chinese or english as given in protocol doc. 
            "remote_server_host_name":"localhost", ## hostname for backend server
            "remote_server_port": 1234, ## port name for backend server
            "socket_timeout_when_no_data_from_server_secs": 5, ## maximum time to wait for any responsne
            "server_connection_retry_time_secs": 15, ## max time to retry connection to the backend server if not connecting
            "login_message_retry_time_secs": 15, ## max time to retry sending login message if not able to send
            "sleep_time_interval_between_retries": 2 ## sleep time between retrying either server connection or any message transmission
        }
    
    It uses a instance of the Concox class present in classess folder which emulates a Concox device.
    All logic is implemented in the Concox class.


### server.py
    This is the process for a Multithreaded TCP server which is accepting connections from terminal devices.
    Each connection to this server creates a new thread and uses a class APP to handle the requests.
    Class APP is impelmented in Classes folder. Class APP contains the main application code whereas server.py works as a simple TCP Listener.
    
### fileStorage folder
    it contains text files where location packets from device are saved.

### classes folder
    it contains class for Concox devices and backend application code.


Run Both of these files in different terminals and Prints statements will convey the status of all the  transactions 
happening between the emulated devices and backend server.


ScreentShot [left device simulator output and right server output]
![Image description](concox.png)