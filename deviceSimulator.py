from classes.Device import Concox

config = {
    "device_imei_str": "123456789123456",
    "device_type_id": "0001",
    "heartbeat_interval_seconds": 5,
    "location_packet_update_interval": 5,
    "timezone_offset": -5,
    "language":"english",
    "remote_server_host_name":"localhost",
    "remote_server_port": 1234,
    "socket_timeout_when_no_data_from_server_secs": 5,
    "server_connection_retry_time_secs": 15,
    "login_message_retry_time_secs": 15,
    "sleep_time_interval_between_retries": 2
}

device = Concox(config)
device.start_simulation()
